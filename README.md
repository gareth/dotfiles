Packages
--------

desktop-base librsvg2-bin plymouth-themes
xserver-xorg-video-all xserver-xorg-core xinit

dpkg-reconfigure keyboard-configuration

$ cd /etc/X11/
# Xorg -configure

xdm (xrandr in Xsetup)

vim
- nano
git

suckless-tools dwm (apt source dwm) libx11-dev libxft-dev libxinerama-dev
---> exec dwm in xsession

stterm
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/local/bin/st 100

-- surf

latex
